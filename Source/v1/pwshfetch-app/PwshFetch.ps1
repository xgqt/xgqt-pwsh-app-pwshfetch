#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

function Format-OSProperty {
    param(
        [string] $Property
    )

    return $Property.Trim('"').Trim()
}

function Get-HostName {
    return [System.Net.Dns]::GetHostName()
}

function Get-OSData {
    $osReleasePath = "/etc/os-release"

    if (Test-Path $osReleasePath) {
        return (Get-Content $osReleasePath | ConvertFrom-StringData)
    }
    else {
        return $null
    }
}

function Get-OS {
    $data = Get-OSData

    if ($null -eq $data) {
        return "N/A"
    }

    $prettyName = Format-OSProperty $data.PRETTY_NAME
    $versionId = Format-OSProperty $data.VERSION_ID

    return "$($prettyName) $($versionId)"
}

function Get-Kernel {
    return "$(uname -r)"
}

function Format-Uptime {
    $uptimeAttributes = Get-Uptime | Select-Object Days, Hours, Minutes, Seconds
    $uptimeArray = @()

    if ($uptimeAttributes.Days -gt 0) {
        $uptimeArray += "$($uptimeAttributes.Days) days"
    }

    if ($uptimeAttributes.Hours -gt 0) {
        $uptimeArray += "$($uptimeAttributes.Hours) hours"
    }

    if ($uptimeAttributes.Minutes -gt 0) {
        $uptimeArray += "$($uptimeAttributes.Minutes) minutes"
    }

    if ($uptimeArray.Length -eq 0) {
        # Display seconds only if we did not even reach to minutes.

        $uptimeString = "$($uptimeAttributes.Seconds) seconds"
    }
    else {
        $uptimeString = ($uptimeArray -join ", ")
    }

    return $uptimeString
}

function Get-SystemPackageCount {
    $data = Get-OSData
    $osId = $data.ID

    $pkgsInstalled = switch -Regex ($osId) {
        ".*(G|g)entoo.*" {
            qlist -Iv
        }
        ".*(D|d)ebian.*" {
            dpkg --list | Select-String -Pattern "^i"
        }
    }

    if ($pkgsInstalled) {
        return ($pkgsInstalled | Measure-Object -Line).Lines
    }

    return "N/A"
}

function Get-ShellName {
    if ($env:SHELL) {
        $shellFileInfo = Get-Item $env:SHELL

        return $shellFileInfo.Name
    }

    return "N/A"
}

function Get-DesktopEnvironment {
    return $env:XDG_CURRENT_DESKTOP `
      ?? $env:DESKTOP_SESSION `
      ?? $env:XDG_SESSION_TYPE `
      ?? "N/A"
}

Write-Output "$($env:USER)@$(Get-HostName)"
Write-Output "OS:       $(Get-OS)"
Write-Output "KERNEL:   $(Get-Kernel)"
Write-Output "UPTIME:   $(Format-Uptime)"
Write-Output "PACKAGES: $(Get-SystemPackageCount)"
Write-Output "SHELL:    $(Get-ShellName)"
Write-Output "DE:       $(Get-DesktopEnvironment)"
