;; -*- no-byte-compile: t -*-

;; Directory Local Variables
;; For more information see (info "(emacs) Directory Variables")

((find-file . ((require-final-newline . t)
               (show-trailing-whitespace . t)))
 (earthfile-mode . ((indent-tabs-mode . nil)
                    (tab-width . 4)))
 (makefile-mode . ((indent-tabs-mode . t)))
 (markdown-mode . ((indent-tabs-mode . nil))))
