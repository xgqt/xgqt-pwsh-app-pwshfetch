# PwshFetch

System information program written in Powershell 7+.

## License

Unless otherwise stated all source code in this repository is licensed under
the [MPL-2.0](https://opensource.org/license/mpl-2-0/) license.

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.
